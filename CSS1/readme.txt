CSS - Cascading Style Sheets

- Serif typefaces: have small decorative lines: Times New Roman, Georgia, Rockwell
- Sans serif typefaces have no decorative lines: Verdana, Arial, Helvetica
- Use a generic font family as the last option, always declare generic fonts without quotes.

- Box-sizing property: 
+ content-box: Default. The width and height properties (and min/max properties) includes only the content. Border, padding, or margin are not included
+ border-box: The width and height properties (and min/max properties) includes content, padding and border, but not the margin
+ initial:  Sets this property to its default value.
+ inherit: Inherits this property from its parent element

